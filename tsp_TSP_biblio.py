# coding: UTF-8
import numpy, pylab, copy

def get_tour_fichier(f):
    """
    :author: DIU-Bloc2 - Univ. Lille
    :date: 2019, Mai
    : lecture fichier de ville format ville, latitude, longitude
    : renvoie un tour contenant les villes dans l ordre du fichier
    : param f: file
    : return : (list)
    """
    fichier = open(f, "r")
    tour_initial = fichier.read().replace (",", ".").split ("\n")
    tour_initial=[ t.strip ("\r\n ").split ("\t") for t in tour_initial ]
    tour_initial = [ t [:1] + [ float (x) for x in t [1:] ] for t in tour_initial ]
    fichier.close()
    return tour_initial[:len(tour_initial)-1]


def distance (tour, i,j) :
    """
    Calcul la distance euclidienne entre i et j
    : param tour: seqeunce de ville
    : param i: numero de la ville de départ
    : param j: numero de la ville d arrivee
    : return: float
    CU: i et j dans le tour
    """
    dx = tour [i][1] - tour [j][1]
    dy = tour [i][2] - tour [j][2]
    return (dx**2 + dy**2) ** 0.5


def longueur_tour (tour) :
    """
    calcul la longueur total d une tournée de la ville de départ et retourne à la ville de départ
    : param tour: tournee de ville n villes = n segments
    : return: float distance totale
    """
    d = 0
    for i in range (0,len(tour)-1) :
        d += distance (tour, i,i+1)
    # il ne faut pas oublier de boucler pour le dernier segment  pour retourner à la ville de départ
    d += distance (tour, 0,-1)
    return d

def trace (tour) :
    """
    Trace la tournée realisée
    : param tour: list de ville
    """
    x = [ t[1] for t in tour ]
    y = [ t[2] for t in tour ] 
    x += [ x [0] ] # on ajoute la dernière ville pour boucler
    y += [ y [0] ] #
    pylab.plot (x,y, linewidth=5)
    for ville,x,y in tour :
        pylab.text (x,y,ville) 
    pylab.show ()

    
def test2():
    """
    Fonction de test des fonctions precedentes
    """
    
    the_tour = get_tour_fichier("exemple.txt")
    print(the_tour)
    print(longueur_tour(the_tour))
    trace(the_tour)

def definition_matrice_distance(liste_villes):
    '''
    retourne une matrice 2*2 contenant les distances
    entre les villes
    : paramètres : (list) la liste des villes
    : return : matrice 2*2 contenant les distances entre les villes
    celles ci étant repérée par leur indice dans le fichier
    '''
    matrice_dist=numpy.zeros((len(liste_villes),len(liste_villes)))
    
    for i in range(len(liste_villes)) :
        for j in range(len(liste_villes)) :
            matrice_dist[i,j]=distance(liste_villes, i,j)
    return matrice_dist

def creation_dictionnaire(liste_villes):
    '''
    création de deux dictionnaires liant indice et ville
    '''
    dict_indice_ville={} # dictionnaire liant l'indice à la ville
    dict_ville_indice={} # dictionnaire liant le nom à l'indice
    for i in range(len(liste_villes)) : # création du contenu des dictionnaires
        dict_indice_ville[i]=liste_villes[i]
        dict_ville_indice[liste_villes[i][0]]=i
    return dict_indice_ville,dict_ville_indice

def ville_la_plus_proche(ville,tour,mat_dist,dict_ville_indice):
    '''
    retourne l'indice de la ville la plus proche
    : ville : (list) ville dont on cherche la plus proche
    : tour : (list) liste des villes
    : mat_dist : (array) matrice des distances
    : return : (int) indice de la ville la plus proche
    '''
    if not(ville in tour) :
        print("Votre ville n'est pas dans le tour")
        return None
    dist_min=numpy.max(mat_dist) # on détermine la distance max
    i=dict_ville_indice[ville[0]] # indice de la ville
    indice_retour=0
    for v in tour : # on parcourt le tour des villes
        j=dict_ville_indice[v[0]]
        dist_i_j=mat_dist[i,j]
        if dist_i_j<dist_min and dist_i_j>0 :
            dist_min=dist_i_j
            indice_retour=j
    return indice_retour

def trajet_voyageur_commerce(ville,tour,mat_dist,dict_ville_indice,dict_indice_ville):
    '''
    retourne la liste des indices des villes pour minimiser
    le parcours selon un algorithme glouton de recherche
    de proche en proche de la ville la plus proche
    : ville : (list) ville de départ
    : tour : (list) liste des villes à parcourir
    : matrice : (array) matrice des distances
    : dict_ville_indice : (dict) dictionnaire des indices
    '''
    liste_retour=[]
    lis_par=copy.deepcopy(tour)
    liste_retour.append(ville)
    while len(liste_retour)<len(tour):
        ville=liste_retour[-1] # on sélectionne le dernier élément
        j=ville_la_plus_proche(ville,lis_par,mat_dist,dict_ville_indice)
        ville_proche=dict_indice_ville[j] # on recupere la ville par son indice
        liste_retour.append(ville_proche) # on l'ajoute
        lis_par.remove(ville) # on supprime la dernière ville testée
    # on boucle la liste avec la premiere ville du parcours
    liste_retour.append(liste_retour[0])
    return liste_retour
    

def fonction_test1():
    '''
    fonction test pour recherche du parcours
    sur toute les villes en partant d'Annecy
    '''
    tour=get_tour_fichier('exemple.txt')
    mat=definition_matrice_distance(tour)
    dict_indice_ville,dict_ville_indice=creation_dictionnaire(tour)
    trajet=trajet_voyageur_commerce(tour[0],tour,mat,dict_ville_indice,dict_indice_ville)
    print("le trajet")
    trace(trajet)
    print("longueur du trajet")
    print(longueur_tour (trajet))
    return

def fonction_test2():
    '''
    fonction test pour recherche du parcours
    sur toute les villes en partant de Lille
    '''
    tour=get_tour_fichier('exemple.txt')
    matrice=definition_matrice_distance(tour)
    dict_indice_ville,dict_ville_indice=creation_dictionnaire(tour)
    trajet=trajet_voyageur_commerce(tour[10],tour,matrice,dict_ville_indice,dict_indice_ville)
    print("le trajet")
    trace(trajet)
    print("longueur du trajet")
    print(longueur_tour (trajet))
    return

def fonction_test3():
    '''
    fonction test pour recherche du parcours
    sur les 13 première villes en partant de Lille
    '''
    tour=get_tour_fichier('exemple.txt')
    tour_reduit=tour[0:12]
    mat=definition_matrice_distance(tour)
    dict_indice_ville,dict_ville_indice=creation_dictionnaire(tour)
    trajet=trajet_voyageur_commerce(tour[10],tour_reduit,mat,dict_ville_indice,dict_indice_ville)
    print("le trajet")
    trace(trajet)
    print("longueur du trajet")
    print(longueur_tour (trajet))

fonction_test1()
fonction_test2()
fonction_test3()





