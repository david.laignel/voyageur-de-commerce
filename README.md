# TP du voyageur de commerce.
Les fonctions fonction_test1(),fonction_test2() et fonction_test3() permettent de tester le programme.
## fonction_test1()
Affiche le tour à partir d'Annecy sur l'ensemble des villes.
## fonction_test2()
Affiche le tour à partir de Lille sur l'ensemble des villes.
## fonction_test3()
Affiche le tour à partir de Lille sur 13 premières villes.
